import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class CycleList<T extends Comparable> {
    private Node head = null;
    private int count = 0;
    private class Node {
        Node next = null;
        Node prev = null;
        T data;

        Node() {
        }

        Node(T d) {
            data = d;
        }
    }

    CycleList() {
    }

    CycleList(CycleList<T> l) {
        Node cur = l.head;
        for (int i = 0; i < l.count; ++i, cur = cur.next) {
            push_back(cur.data);
        }
    }

    public boolean is_empty() {
        return head == null;
    }

    public int length() {
        return count;
    }

    private Node get_node(int idx) {
        int half_count = Math.floorDiv(count, 2);
        Node cur = head;
        if (idx <= half_count)
            while (idx-- != 0)
                cur = cur.next;
        else {
            idx = count - idx;
            while (idx-- != 0)
                cur = cur.prev;
        }
        return cur;
    }

    private void unlink_node(Node cur) {
        --count;
        cur.prev.next = cur.next;
        cur.next.prev = cur.prev;
    }

    public boolean push_front(T el) {
        if (!push_back(el)) return false;
        head = head.prev;
        return true;
    }

    public boolean push_back(T el) {
        Node q = new Node(el);
        q.next = q.prev = q;
        ++count;
        if (is_empty()) {
            head = q;
            return true;
        }
        q.next = head;
        q.prev = head.prev;
        head.prev.next = q;
        head.prev = q;
        return true;
    }

    public T at(int idx) {
        if (idx >= count || idx < 0) throw new IndexOutOfBoundsException();
        Node cur = get_node(idx);
        return cur.data;
    }

    public T remove(int idx) {
        if (idx >= count || idx < 0)
            throw new IndexOutOfBoundsException();
        Node cur = get_node(idx);
        unlink_node(cur);
        if (head == cur) head = cur.next;
        if (head == cur) head = null;
        return cur.data;
    }

    public boolean insert(T el, int idx) {
        if (idx > count || idx < 0)
            throw new IndexOutOfBoundsException();
        Node q = new Node(el);
        q.next = q.prev = q;
        ++count;
        if (is_empty()) {
            head = q;
            return true;
        }

        Node prev = get_node(idx - 1);
        q.next = prev.next;
        q.prev = prev;
        prev.next = q;
        q.next.prev = q;
        if (idx == 0)
            head = q;
        return true;
    }

    public T pop_back() {
        if (is_empty())
            throw new NoSuchElementException();
        Node cur = head.prev;
        unlink_node(cur);
        if (head == cur) head = cur.next;
        if (head == cur) head = null;
        return cur.data;
    }

    public T pop_front() {
        if (is_empty())
            throw new NoSuchElementException();
        Node cur = head;
        unlink_node(cur);
        head = cur.next;
        if (head == cur) head = null;
        return cur.data;
    }

    public void for_each(Consumer<T> action) {
        Node cur = head;
        for (int i = 0; i < count; ++i) {
            action.accept(cur.data);
            cur = cur.next;
        }
    }

    private void swap(Node l, Node r) {
        if (r.next == r.prev) {
            head = r;
        }
        else {
            l.prev.next = r;
            l.next = r.next;
            r.prev = l.prev;
            r.next.prev = l;
            l.prev = r;
            r.next = l;
            if (l == head)
                head = r;
        }
    }

    public void sort(Comparator<T> comparator) {
        if (is_empty())
            return;
        Node cur_i = head.next;
        while (cur_i != head) {
            Node cur_j = cur_i;
            while (cur_j != head) {
                if (comparator.compare(cur_j.prev.data, cur_j.data) > 0) {
                    swap(cur_j.prev, cur_j);
                    if (cur_i == cur_j)
                        cur_i = cur_i.next;
                }
                else
                    break;
            }
            cur_i = cur_i.next;
        }
    }

//    ArrayList<CycleList<T>> split() {
//        Node cur = head;
//        CycleList<T> first = new CycleList<>();
//        CycleList<T> second = new CycleList<>();
//        int half_count = Math.floorDiv(count, 2);
//        int i = 0;
//        while (i++ < half_count) {
//            first.push_back(cur.data);
//            cur = cur.next;
//        }
//        while (i++ < count) {
//            second.push_back(cur.data);
//            cur = cur.next;
//        }
//        ArrayList<CycleList<T>> ret = new ArrayList<>(2);
//        ret.add(first);
//        ret.add(second);
//        return ret;
//    }

    public CycleList<T> filter(Predicate<T> p) {
        Node cur = head;
        CycleList<T> ret = new CycleList<>();
        for (int i = 0; i < count; ++i, cur = cur.next)
            if (p.test(cur.data))
                ret.push_back(cur.data);
        return ret;
    }

    public CycleList<T> append(CycleList<T> l) {
        while (l.count != 0) {
            push_back(l.pop_front());
        }
        return this;
    }

//    void merge(Comparator<T> comparator, CycleList<T> l) {
//        while (l.count != 0) {
//            return;
//        }
//    }
//
//    void merge_sort(Comparator<T> comparator) {
//        ArrayList<CycleList<T>> lists = this.split();
//        lists.get(0).merge(comparator, lists.get(1));
//        this.head = lists.get(0).head;
////        merge(comparator, );
//    }

    private CycleList<T> q_sort_helper(Comparator<T> comparator) {
        if (is_empty() || count == 1) return this;
        T pivot = head.data;
        return  filter((val) -> comparator.compare(val, pivot) < 0).q_sort_helper(comparator)
                .append(filter((val) -> 0 == comparator.compare(val, pivot)))
                .append(filter((val) -> comparator.compare(val, pivot) > 0).q_sort_helper(comparator));
    }

    public void q_sort(Comparator<T> comparator) {
        if (is_empty() || count == 1) return;
        head = q_sort_helper(comparator).head;
    }
}
