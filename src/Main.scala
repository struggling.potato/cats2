import java.util.function.Consumer
import java.util.{Comparator, Random}

import cats.SCycleList

object Main {

  val random = new Random(13)



  def genStr: String = {
    val letters = "abcdefghijklmnopqrstuvwxyz"
    //final String CHAR_UPPER = letters.toUpperCase();
    val number = "0123456789"
    val data = letters + number
    /*CHAR_UPPER*/
    var length = random.nextInt(5)
    length += 5
    val sb = new StringBuilder(length)
    var i = 0
    while (i < length) {
      val rnd = random.nextInt(data.length)
      val rndChar = data.charAt(rnd)
      sb.append(rndChar)
      i += 1
    }
    sb.toString()
  }

  def main(args: Array[String]): Unit = {
    val mprint = new Consumer[String] {
      override def accept(t: String) = System.out.print(t + " ")}
    val comp = new Comparator[String] {
      override def compare(o1: String, o2: String) = o1.toString.compareToIgnoreCase(o2.toString)}
    var i = 0
    var list = new SCycleList[String]
    while ( { i < 1
    }) {
      list.push_back(genStr)
      i += 1
    }
    list.for_each(mprint)
    System.out.println("")
    list.q_sort(comp)
    list.for_each(mprint)
    System.out.println("")
    System.out.println(list.is_empty)
    System.out.println(list.length)
    System.out.println(list.at(3))
    System.out.println(list.remove(2))
    System.out.println(list.pop_back)
    System.out.println(list.pop_front)
    list.q_sort(comp)
    list.for_each(mprint)
    System.out.println("")
    i = 0
    while ( {
      i < 90
    }) {
      list.push_back(genStr)
      i += 1
    }
    list.push_front(genStr)
    list.for_each(mprint)
    System.out.println("")
    System.out.println(list.is_empty)
    System.out.println(list.length)
    System.out.println(list.at(3))
    System.out.println(list.at(12))
    System.out.println(list.remove(2))
    System.out.println(list.remove(17))
    System.out.println(list.insert(genStr, 17))
    System.out.println(list.insert(genStr, 5))
    list.for_each(mprint)
    System.out.println("")
    System.out.println(list.pop_back)
    System.out.println(list.pop_front)
    System.out.println(list.length)
    System.out.println("")
    list.for_each(mprint)
    System.out.println("")
    list = new SCycleList[String]
    val list2 = new CycleList[String]
    i = 0
    while ( {
      i < 10
    }) {
      var s = genStr
      list.push_back(s)
      list2.push_back(s)
      i += 1
    }
    list.for_each(mprint)
    println()
    var before = System.nanoTime
    list.q_sort(comp)
    var after = System.nanoTime
    list.for_each(mprint)
    System.out.println("")
    System.out.println("Time scala q_sort: " + (after - before).toDouble / 1000000 + "ms")
    before = System.nanoTime
    list2.q_sort(comp)
    after = System.nanoTime
    list2.for_each(mprint)
    System.out.println("")
    System.out.println("Time q_sort: " + (after - before).toDouble / 1000000 + "ms")
  }




//  def main(args: Array[String]): Unit = {
//    print("Hello, world!")
//  }
}